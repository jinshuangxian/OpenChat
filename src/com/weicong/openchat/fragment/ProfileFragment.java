package com.weicong.openchat.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.ui.entity.KFVCardEntity;
import com.appkefu.lib.utils.KFImageUtils;
import com.weicong.openchat.R;
import com.weicong.openchat.activity.ChangePasswordActivity;
import com.weicong.openchat.activity.ProfileChangeActivity;
import com.weicong.openchat.activity.QrCodeActivity;
import com.weicong.openchat.activity.SelectPictureActivity;
import com.weicong.openchat.activity.SettingsActivity;
import com.weicong.openchat.activity.ShowImageActivity;
import com.weicong.openchat.activity.WelcomeActivity;
import com.ypy.eventbus.EventBus;

public class ProfileFragment extends Fragment {

	private final int REQUEST_NICKNAME = 100;
	private final int REQUEST_SIGNATURE = 101;
	
	public static final String DATA = "my_data";
	
	private KFVCardEntity mVcardEntity;
	
	private ImageView mHeadView;
	private TextView mUsername;
	private TextView mNickname;
	private TextView mSignature;
	
	private String mPath;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mVcardEntity = KFIMInterfaces.getVCard();
		
		EventBus.getDefault().register(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_profile, null);
		
		mHeadView = (ImageView) v.findViewById(R.id.iv_image);
		mHeadView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), ShowImageActivity.class);
				intent.putExtra(ShowImageActivity.PATH, mPath);
				startActivity(intent);
			}
		});
		
		mUsername = (TextView) v.findViewById(R.id.tv_username);
		mUsername.setText(KFSettingsManager.getSettingsManager(getActivity()).getUsername());
		
		mNickname = (TextView) v.findViewById(R.id.tv_nickname);
		mSignature = (TextView) v.findViewById(R.id.tv_sign);
		if (mVcardEntity != null) {
		
			mNickname.setText(mVcardEntity.getNickname());
			mSignature.setText(mVcardEntity.getSignature());
		}
		
		v.findViewById(R.id.rl_change).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				KFIMInterfaces.Logout(getActivity());
				Intent intent = new Intent(getActivity(), WelcomeActivity.class);
				startActivity(intent);
				getActivity().finish();
			}
		});
		
		v.findViewById(R.id.rl_exit).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				KFIMInterfaces.Logout(getActivity());
				getActivity().finish();
			}
		});
		
		v.findViewById(R.id.rl_nickname).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), ProfileChangeActivity.class);
				intent.putExtra(ProfileChangeActivity.TYPE, ProfileChangeActivity.CHANGE_NICKNAME);
				intent.putExtra(ProfileChangeActivity.DATA, mVcardEntity.getNickname());
				startActivityForResult(intent, REQUEST_NICKNAME);
			}
		});
		
		v.findViewById(R.id.rl_sign).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), ProfileChangeActivity.class);
				intent.putExtra(ProfileChangeActivity.TYPE, ProfileChangeActivity.CHANGE_SIGNATURE);
				intent.putExtra(ProfileChangeActivity.DATA, mVcardEntity.getSignature());
				startActivityForResult(intent, REQUEST_SIGNATURE);
			}
		});
		
		v.findViewById(R.id.rl_image).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), SelectPictureActivity.class);
				startActivity(intent);
				
			}
		});
		
		v.findViewById(R.id.rl_password).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
				startActivity(intent);
			}
		});
		
		v.findViewById(R.id.rl_settings).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), SettingsActivity.class);
				startActivity(intent);
				
			}
		});
		
		v.findViewById(R.id.rl_username).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(), QrCodeActivity.class);
				intent.putExtra(QrCodeActivity.PATH, mPath);
				startActivity(intent);
				
			}
		});
		
		return v;
	}
	
	public void onEventMainThread(Event event) {  
		mPath = event.getPath();
		KFIMInterfaces.setHeadImage(getActivity(), mPath);
		
		Bitmap bitmap = KFImageUtils.loadImgThumbnail(mPath, 100, 100);
		if(bitmap != null)
			mHeadView.setImageBitmap(bitmap);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == REQUEST_NICKNAME && resultCode == getActivity().RESULT_OK) {
			mNickname.setText(data.getStringExtra(DATA));
		} else if (requestCode == REQUEST_SIGNATURE && resultCode == getActivity().RESULT_OK) {
			mSignature.setText(data.getStringExtra(DATA));
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onResume() {
		super.onResume();	
		KFIMInterfaces.getHeadImage(getActivity());
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(KFMainService.ACTION_IM_GET_AVATAR_RESULT);
        getActivity().registerReceiver(mXmppreceiver, intentFilter);        
	}
	
	@Override
	public void onStop() {
		super.onStop();

        getActivity().unregisterReceiver(mXmppreceiver);
	}
	
	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this); //���ע��
		super.onDestroy();
	}
	
	public static class Event {
		private String mPath;
		public Event(String path) {
			mPath = path;
		}
		
		public String getPath() {
			return mPath;
		}
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() 
	{
        public void onReceive(Context context, Intent intent) 
        {
            String action = intent.getAction();

            if(action.equals(KFMainService.ACTION_IM_GET_AVATAR_RESULT))
            {
            	mPath = intent.getStringExtra("path");
            	
            	Bitmap bitmap = KFImageUtils.loadImgThumbnail(mPath, 100, 100);
        		if(bitmap != null)
        			mHeadView.setImageBitmap(bitmap);
        		
            }
        }
    };
}
