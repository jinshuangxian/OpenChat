package com.weicong.openchat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.service.KFXmppManager;
import com.appkefu.lib.utils.KFSLog;
import com.weicong.openchat.R;
import com.weicong.openchat.utils.ToolUtil;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class LoginActivity extends Activity {

	private EditText mUsernameText;
	private EditText mPasswordText;
	
	private ProgressDialog mProgressDialog;
	
	private boolean mError = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mProgressDialog = ToolUtil.showProgressDialog(this, "正在登陆...");
		
		mUsernameText = (EditText) findViewById(R.id.et_username);
		mPasswordText = (EditText) findViewById(R.id.et_password);
		
		Button loginButton = (Button) findViewById(R.id.btn_login);
		loginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				validate();
			}
		});
		ToolUtil.pushActivity(this);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(KFMainService.ACTION_XMPP_CONNECTION_CHANGED);
        registerReceiver(mXmppreceiver, intentFilter);        

	}
	
	@Override
	protected void onStop() {
		super.onStop();

        unregisterReceiver(mXmppreceiver);
	}
	
	@Override
	protected void onDestroy() {
		Crouton.clearCroutonsForActivity(this);
		ToolUtil.removeActivity(this);
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * 验证
	 */
	private void validate() {
		String username = mUsernameText.getText().toString();
		String password = mPasswordText.getText().toString();
		
		if (username.equals("")) {
			mUsernameText.setError("请输入用户名");
			return;
		}
		if (password.equals("")) {
			mPasswordText.setError("请输入密码");
			return;
		}
		if (username.length() < 6) {
			mUsernameText.setError("少于6位数");
			return;
		}
		if (password.length() < 6) {
			mPasswordText.setError("少于6位数");
			return;
		}
		
		login(username, password);
	}
	
	/**
	 * 登录
	 */
	private void login(String username, String password) {
		mError = false;
		KFIMInterfaces.login(this, username, password);
		mProgressDialog.show();
		hideSoftInputView();
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() 
	{
        public void onReceive(Context context, Intent intent) 
        {
            String action = intent.getAction();
            if (action.equals(KFMainService.ACTION_XMPP_CONNECTION_CHANGED) && !mError) 
            {
                updateStatus(intent.getIntExtra("new_state", 0));   
            }
           
        }
    };
    
    private void updateStatus(int status) {
        switch (status) {
            case KFXmppManager.CONNECTED:
            	KFSLog.d("登录成功");
            	mProgressDialog.dismiss();
            	loginSuccess();
            	
                break;
            case KFXmppManager.DISCONNECTED:
            	KFSLog.d("未登录");  
                break;
            case KFXmppManager.CONNECTING:
            	KFSLog.d("登录中");
            	break;
            case KFXmppManager.DISCONNECTING:
            	KFSLog.d("登出中");
            	Crouton.makeText(this, "用户名或密码错误", Style.CONFIRM).show();
                mProgressDialog.dismiss();
            	mError = true;
                break;
            case KFXmppManager.WAITING_TO_CONNECT:
            case KFXmppManager.WAITING_FOR_NETWORK:
            	KFSLog.d("waiting to connect");
                break;
            default:
                break;
        }
    }
    
    /**
     * 登录成功
     */
    private void loginSuccess() {
    	Intent intent = new Intent(this, MainActivity.class);
    	startActivity(intent);
    	ToolUtil.clearActivitys(this);
    }
    
    /**
     * 隐藏软键盘
     */
    private void hideSoftInputView() {
		InputMethodManager manager = ((InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE));
		if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (getCurrentFocus() != null)
				manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
}
