package com.weicong.openchat.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.weicong.openchat.R;
import com.weicong.openchat.fragment.ProfileFragment;

public class ProfileChangeActivity extends Activity {

	public static final String CHANGE_NICKNAME = "NICKNAME";
	public static final String CHANGE_SIGNATURE = "SIGNATURE";
	
	public static final String DATA = "data";
	public static final String TYPE = "type";
	
	private String mType;
	private String mData;
	
	private EditText mChangeText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_change);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mChangeText = (EditText) findViewById(R.id.et_change);
		
		Intent intent = getIntent();
		mType = intent.getStringExtra(TYPE);
		mData = intent.getStringExtra(DATA);
		if (mType.equals(CHANGE_NICKNAME)) {
			setTitle("更改昵称");
			if (mData == null) {
				mChangeText.setHint("输入昵称");
			} else {
				mChangeText.setText(mData);
				mChangeText.setSelection(mData.length());
			}
		} else {
			setTitle("更改个性签名");
			if (mData == null) {
				mChangeText.setHint("输入个性签名");
			} else {
				mChangeText.setText(mData);
				mChangeText.setSelection(mData.length());
			}
		}
		
		Button saveButton = (Button) findViewById(R.id.btn_save);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String data = mChangeText.getText().toString();
				if (data.equals("")) {
					mChangeText.setError("不能为空");
				} else {
					KFIMInterfaces.setVCardField(ProfileChangeActivity.this, mType, data);
					Intent intent = new Intent();
					intent.putExtra(ProfileFragment.DATA, data);
					setResult(RESULT_OK, intent);
					finish();
				}
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	} 
}
