package com.weicong.openchat.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.utils.KFImageUtils;
import com.appkefu.lib.utils.KFUtils;
import com.appkefu.lib.xmpp.XmppVCard;
import com.weicong.openchat.R;
import com.weicong.openchat.adapter.ChatAdapter;
import com.weicong.openchat.entity.MessageEntity;
import com.weicong.openchat.utils.RobotUtil;

public class RobotChatActivity extends Activity {

	private Handler mMainHandler;
	
	private HandlerThread mThread;
	private Handler mThreadHandler;
	
	private EditText mInputText;
	
	private ListView mListView;
	private ChatAdapter mChatAdapter;
	private List<MessageEntity> mMessageList = new ArrayList<MessageEntity>();
	
	private Bitmap mSendBitmap;
	private Bitmap mReceiveBitmap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_robot_chat);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("ͼ�������");
		
		mMainHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				Log.d("weicong", msg.obj.toString());
				String text = RobotUtil.getText(msg.obj.toString());
				MessageEntity entity = new MessageEntity();
            	entity.setContent(text);
            	entity.setDate(KFUtils.getDate());
            	entity.setType(0);
            	
            	mMessageList.add(entity);
            	mChatAdapter.notifyDataSetChanged();
			}
		};
		
		mThread = new HandlerThread("HandlerThread");
		mThread.start();
		mThreadHandler = new Handler(mThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				String info = (String)msg.obj;
				String text = RobotUtil.getResponse(info);
				mMainHandler.obtainMessage(0, text).sendToTarget();
			}
		};
		
		String sendPath = XmppVCard.getOthersAvatarPath(KFSettingsManager.getSettingsManager(this).getUsername());
		mSendBitmap = KFImageUtils.getBitmapByPath(sendPath);
		if (mSendBitmap == null) {
			mSendBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.head);
		}
	
		mReceiveBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.robot);
		
		mListView = (ListView) findViewById(R.id.listView);
		mChatAdapter = new ChatAdapter(this, mMessageList, mSendBitmap, mReceiveBitmap);
		mListView.setAdapter(mChatAdapter);
		
		mInputText = (EditText) findViewById(R.id.appkefu_inputbar_edittext);
		
		TextView sendText = (TextView) findViewById(R.id.tv_send);
		sendText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String info = mInputText.getText().toString();
				if (info.equals("")) {
					mInputText.setError("����������");
				} else {
					mThreadHandler.obtainMessage(0, info).sendToTarget();
					MessageEntity entity = new MessageEntity();
					entity.setContent(info);
	            	entity.setDate(KFUtils.getDate());
	            	entity.setType(1);
	            	mMessageList.add(entity);
	            	mChatAdapter.notifyDataSetChanged();
					mInputText.setText("");
				}
			}
		});
	}
	
	
	@Override
	protected void onDestroy() {
		mThread.quit();
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
