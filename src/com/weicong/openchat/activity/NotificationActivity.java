package com.weicong.openchat.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appkefu.lib.db.KFConversationHelper;
import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.weicong.openchat.R;

public class NotificationActivity extends Activity {

	public static final String USERNAME = "username";
	public static final String TIME = "time";
	
	private String mUsername;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("验证消息");
		
		Intent intent = getIntent();
		mUsername = intent.getStringExtra(USERNAME);
		String time = intent.getStringExtra(TIME);
		
		TextView timeText = (TextView) findViewById(R.id.tv_time);
		timeText.setText(time);
		
		TextView messageText = (TextView) findViewById(R.id.tv_message);
		String message = "用户名为" + mUsername + "请求添加您为好友";
		messageText.setText(message);
		
		Button agreeButton = (Button) findViewById(R.id.btn_agree);
		agreeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				KFIMInterfaces.addFriend(NotificationActivity.this, mUsername, "");
				KFConversationHelper.getConversationHelper(
						getApplicationContext()).deleteConversation("appkefu_subscribe_notification_message");
				finish();
			}
		});
		
		Button cancelButton = (Button) findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
