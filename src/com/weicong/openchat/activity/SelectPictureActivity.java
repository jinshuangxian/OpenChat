package com.weicong.openchat.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;
import com.weicong.openchat.R;

public class SelectPictureActivity extends Activity {

	private static final int REQUEST_CAMERA_IMAGE = 1;
	
	private ArrayList<String> mImages = new ArrayList<String>();
	
	private ImageLoader mImageLoader;
	private DisplayImageOptions mOptions; // DisplayImageOptions是用于设置图片显示的类
	
	private GridView mGridView;
	private ImageAdapter mImageAdapter;
	
	private int mScreenWidth;
	
	private File mSaveHeadImageFile; //调用相机时头像保存的路径
	
	private boolean mFromChat = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_picture);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("图片");
		
		mFromChat = getIntent().getBooleanExtra(ChatActivity.FLAG, false);
		
		mImageLoader = ImageLoader.getInstance();
		mOptions = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.no_picture)		// 设置图片下载期间显示的图片
			.cacheInMemory(true)						// 设置下载的图片是否缓存在内存中
			.cacheOnDisc(true)							// 设置下载的图片是否缓存在SD卡中 
			.build();
		
		DisplayMetrics dm = new DisplayMetrics();  
        getWindowManager().getDefaultDisplay().getMetrics(dm);  
        mScreenWidth = dm.widthPixels;// 获取分辨率宽度
		
		mGridView = (GridView) findViewById(R.id.gridView);
		mGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				if (position == 0) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra("autofocus", true);//自动对焦  
		            intent.putExtra("fullScreen", false);//全屏  
		            intent.putExtra("showActionIcons", false);  
		            String filename = UUID.randomUUID().toString() + ".jpg";
		            mSaveHeadImageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), filename);
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mSaveHeadImageFile));
					startActivityForResult(intent, REQUEST_CAMERA_IMAGE);
					
				} else {
					if (!mFromChat) {
						Intent intent = new Intent(SelectPictureActivity.this, ClipPictureActivity.class);
						intent.putExtra(ClipPictureActivity.PATH, mImages.get(position));
						startActivity(intent);
						finish();
					} else {
						Intent intent = new Intent();
						intent.putExtra("path", mImages.get(position));
						setResult(RESULT_OK, intent);
						finish();
					}
				}
			}
		});
		
		getImages();
		
		mImageAdapter = new ImageAdapter(mImages);
		mGridView.setAdapter(mImageAdapter);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			AnimateFirstDisplayListener.displayedImages.clear();
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//使用相机拍摄返回的图片
		if (requestCode == REQUEST_CAMERA_IMAGE && resultCode == RESULT_OK) {
			scan();
			if (!mFromChat) {
				Intent intent = new Intent(this, ClipPictureActivity.class);
				intent.putExtra(ClipPictureActivity.PATH, mSaveHeadImageFile.getAbsolutePath());
				startActivity(intent);
				finish();
			} else {
				Intent intent = new Intent();
				intent.putExtra("path", mSaveHeadImageFile.getAbsolutePath());
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
	
	//将图片图片添加到媒体库
	private void scan() {
		MediaScannerConnection.scanFile(this, new String[] { mSaveHeadImageFile.toString() }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                
            }
        });

	}
	
	private void getImages() {
		Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI, null, null, null, null);
		String path;
		mImages.add("R.drawable.camera_icon");
		while (cursor.moveToNext()) {
			byte[] data = cursor.getBlob(cursor.getColumnIndex(Media.DATA));
			path = new String(data, 0, data.length-1);
			mImages.add(path);
		}
	}
	
	private class ImageAdapter extends ArrayAdapter<String> {
		
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		
		public ImageAdapter(ArrayList<String> images) {
			super(SelectPictureActivity.this, 0, images);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.select_picture_item, null);
				viewHolder = new ViewHolder();
				
				viewHolder.imageView = (ImageView)convertView.findViewById(R.id.iv_image);
				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, (int)(mScreenWidth / 3.0));
				viewHolder.imageView.setLayoutParams(params);
				
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder)convertView.getTag();
			}
			
			String url = null;
			if (position == 0) {
				url = "drawable://" + R.drawable.camera_icon;
			} else {
				url = Scheme.FILE.wrap(mImages.get(position));
			}
			
			/**
			 * 显示图片
			 * 参数1：图片url
			 * 参数2：显示图片的控件
			 * 参数3：显示图片的设置
			 * 参数4：监听器
			 */
			mImageLoader.displayImage(url, viewHolder.imageView, mOptions, animateFirstListener);
			
			return convertView;
		}
	}
	
	private class ViewHolder {
		public ImageView imageView;
	}
	
	/**
	 * 图片加载第一次显示监听器
	 * @author Administrator
	 *
	 */
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {
		
		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				// 是否第一次显示
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					// 图片淡入效果
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}
