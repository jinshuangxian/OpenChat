package com.weicong.openchat.activity;

import uk.co.senab.photoview.PhotoViewAttacher;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.appkefu.lib.utils.KFImageUtils;
import com.weicong.openchat.R;

public class ShowImageActivity extends Activity {

	public static final String PATH = "path";
	
	private PhotoViewAttacher mAttacher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_image);
		
		String path = getIntent().getStringExtra(PATH);
		Bitmap bitmap = KFImageUtils.loadImgThumbnail(path, 300, 300);
		
		ImageView mImageView = (ImageView) findViewById(R.id.iv_photo);
		if (bitmap == null) {
			mImageView.setImageResource(R.drawable.head);
		} else {
			mImageView.setImageBitmap(bitmap);
		}
		
		mAttacher = new PhotoViewAttacher(mImageView);
		
	}
	
	@Override
    public void onDestroy() {
        super.onDestroy();

        // Need to call clean-up
        mAttacher.cleanup();
    }
}
