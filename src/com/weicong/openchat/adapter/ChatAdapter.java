package com.weicong.openchat.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;
import com.weicong.openchat.R;
import com.weicong.openchat.activity.ShowImageActivity;
import com.weicong.openchat.entity.MessageEntity;
import com.weicong.openchat.face.FaceUtil;
import com.weicong.openchat.utils.ImageUtil;
import com.weicong.openchat.video.PlayVideoActivity;
import com.weicong.openchat.video.VideoUtil;
import com.weicong.openchat.voice.MediaManager;
import com.weicong.openchat.voice.VoiceUtil;

public class ChatAdapter extends BaseAdapter {

	private final int SEND = 1;
	private final int RECEIVE = 0;
	
	public List<MessageEntity> mList;
	
	private Activity mActivity;
	
	private Bitmap mSendBitmap;
	private Bitmap mReceiveBitmap;
	
	private ImageLoader mImageLoader;
	private DisplayImageOptions mOptions; // DisplayImageOptions是用于设置图片显示的类
	
	public ChatAdapter(Activity activity, List<MessageEntity> list, 
			Bitmap sendBitmap, Bitmap receiveBitmap) {
		mList = list;
		mActivity = activity;
		mSendBitmap = sendBitmap;
		mReceiveBitmap = receiveBitmap;
		
		mImageLoader = ImageLoader.getInstance();
		mOptions = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.no_picture)		// 设置图片下载期间显示的图片
			.cacheInMemory(true)						// 设置下载的图片是否缓存在内存中
			.cacheOnDisc(true)							// 设置下载的图片是否缓存在SD卡中 
			.build();
	}
	
	@Override
	public int getCount() {
		
		return mList.size();
	}

	@Override
	public MessageEntity getItem(int arg0) {
		return mList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	@Override
	public int getItemViewType(int position) {
		return mList.get(position).getType();
	}

	@Override
	public int getViewTypeCount() {
		return 2;// size要大于布局个数
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int type = getItemViewType(position);
		ViewHolder viewHolder = null;
		
		if (convertView == null) {
			
			if (type == SEND) {
				convertView = mActivity.getLayoutInflater().inflate(R.layout.chat_message_to, null);	
			} else if (type == RECEIVE) {
				convertView = mActivity.getLayoutInflater().inflate(R.layout.chat_message_from, null);
			}
			
			viewHolder = new ViewHolder();
			
			viewHolder.timestamp = (TextView) convertView.findViewById(R.id.tv_timestamp);
			viewHolder.head = (ImageView) convertView.findViewById(R.id.iv_head);
			viewHolder.content = (TextView) convertView.findViewById(R.id.tv_content);
			viewHolder.voiceLayout = (FrameLayout) convertView.findViewById(R.id.voice_layout);
			viewHolder.voiceLength = (TextView) convertView.findViewById(R.id.tv_voice_length);
			viewHolder.videoLayout = (FrameLayout) convertView.findViewById(R.id.video_layout);
			viewHolder.imageLayout = (FrameLayout) convertView.findViewById(R.id.image_layout);
			viewHolder.image = (ImageView) convertView.findViewById(R.id.iv_image);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		final int[] voiceRes = new int[2];
		
		if (type == SEND) {
			viewHolder.head.setImageBitmap(mSendBitmap);
			
			voiceRes[0] = R.anim.send_play_audio;
			voiceRes[1] = R.drawable.voice_right;
		
		} else {
			viewHolder.head.setImageBitmap(mReceiveBitmap);
			
			voiceRes[0] = R.anim.receive_play_audio;
			voiceRes[1] = R.drawable.voice_left;
		}
		
		MessageEntity entity = getItem(position);
		
		String content = entity.getContent();
		
		
		if (content.contains(VoiceUtil.VOICE_TAG)) { //语音信息
			
			viewHolder.content.setVisibility(View.GONE);
			viewHolder.videoLayout.setVisibility(View.GONE);
			viewHolder.imageLayout.setVisibility(View.GONE);
			viewHolder.voiceLayout.setVisibility(View.VISIBLE);
			viewHolder.voiceLength.setVisibility(View.VISIBLE);
			
			float length = VoiceUtil.getVoiceLength(content);
			viewHolder.voiceLength.setText(Math.round(length) + "\"");
			
			final View voiceAnim = convertView.findViewById(R.id.voiceAnim);
			final String filePath = VoiceUtil.getVoiceContentPath(content);
			viewHolder.voiceLayout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					voiceAnim.setBackgroundResource(voiceRes[0]);
					AnimationDrawable animation = (AnimationDrawable) voiceAnim.getBackground();
					animation.start();
					// 播放音频
					MediaManager.playSound(filePath,
							new MediaPlayer.OnCompletionListener() {
								@Override
								public void onCompletion(MediaPlayer mp) {
									voiceAnim.setBackgroundResource(voiceRes[1]);
								}
							});
				}
			});
			
		} else if (content.contains(VideoUtil.VIDEO_TAG)) { //视频信息
			viewHolder.content.setVisibility(View.GONE);
			viewHolder.voiceLayout.setVisibility(View.GONE);
			viewHolder.voiceLength.setVisibility(View.GONE);
			viewHolder.imageLayout.setVisibility(View.GONE);
			viewHolder.videoLayout.setVisibility(View.VISIBLE);
			
			final String path = VideoUtil.getVideoContentPath(content);
			
			viewHolder.videoLayout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(mActivity, PlayVideoActivity.class);
					intent.putExtra(PlayVideoActivity.PATH, path);
					mActivity.startActivity(intent);
				}
			});
			
		} else if (content.contains(ImageUtil.IMAGE_TAG)) { //图片信息
			viewHolder.voiceLayout.setVisibility(View.GONE);
			viewHolder.voiceLength.setVisibility(View.GONE);
			viewHolder.videoLayout.setVisibility(View.GONE);
			viewHolder.content.setVisibility(View.GONE);
			viewHolder.imageLayout.setVisibility(View.VISIBLE);
			
			final String path = ImageUtil.getImageContentPath(content);
			final String url = Scheme.FILE.wrap(path);
			/**
			 * 显示图片
			 * 参数1：图片url
			 * 参数2：显示图片的控件
			 * 参数3：显示图片的设置
			 */
			mImageLoader.displayImage(url, viewHolder.image, mOptions);
			viewHolder.imageLayout.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(mActivity, ShowImageActivity.class);
					intent.putExtra(ShowImageActivity.PATH, path);
					mActivity.startActivity(intent);	
				}
			});
			
		} else { //文本信息
			
			viewHolder.voiceLayout.setVisibility(View.GONE);
			viewHolder.voiceLength.setVisibility(View.GONE);
			viewHolder.videoLayout.setVisibility(View.GONE);
			viewHolder.imageLayout.setVisibility(View.GONE);
			viewHolder.content.setVisibility(View.VISIBLE);
			
			SpannableStringBuilder sb = FaceUtil.stringToFace(mActivity, viewHolder.content, content);
			viewHolder.content.setText(sb);
		}
		viewHolder.timestamp.setText(entity.getDate());
		
		return convertView;
	}

	private class ViewHolder {
		public TextView timestamp;
		public ImageView head;
		public TextView content;
		
		public FrameLayout voiceLayout;
		public TextView voiceLength;
		
		public FrameLayout videoLayout;
		
		public FrameLayout imageLayout;
		public ImageView image;
	}
}
