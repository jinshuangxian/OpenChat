package com.weicong.openchat.voice;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import android.os.Environment;
import android.util.Base64;

public class VoiceUtil {
	public static final String VOICE_TAG = "!#apssawwadpadffs!#";
	
	public static String getVoiceSendMsg(float time, String filePath) {
		try {
			File file = new File(filePath);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			byte[] data = new byte[1024];
			FileInputStream fis = new FileInputStream(file); 
			int len = 0;
			while((len = fis.read(data)) != -1)
			{               
				byteArrayOutputStream.write(data, 0, len);           
			}       
			String content = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 
					Base64.DEFAULT);
			fis.close();
			byteArrayOutputStream.close();
			StringBuilder sb = new StringBuilder();
			sb.append(Float.toString(time));
			sb.append(VOICE_TAG);
			sb.append(content);
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getVoiceMsg(float time, String filePath) {
		StringBuilder sb = new StringBuilder();
		sb.append(Float.toString(time));
		sb.append(VOICE_TAG);
		sb.append(filePath);
		return sb.toString();
	}
	
	public static String getVoiceContentPath(String msg) {
		String[] array = msg.split(VOICE_TAG);
		return array[1];
	}
	
	public static String saveVoice(String msg) {
		String[] array = msg.split(VOICE_TAG);
		String content = array[1];
		String length = array[0];
		byte[] data = Base64.decode(content, Base64.DEFAULT);
		String dir = Environment.getExternalStorageDirectory()
				+ "/openchat/voice";
		File fileDir = new File(dir);
		if (!fileDir.exists())
			fileDir.mkdirs();
		String fileName = generateFileName();
		File file = new File(fileDir, fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		msg = length + VOICE_TAG + file.getAbsolutePath();
		return msg;
	}
	
	public static float getVoiceLength(String msg) {
		String[] array = msg.split(VOICE_TAG);
		String length = array[0];
		return Float.valueOf(length);
	}
	
	/**
	 * 随机生成文件名称
	 * 
	 * @return
	 */
	private static String generateFileName() {
		return UUID.randomUUID().toString() + ".amr";
	}
}
