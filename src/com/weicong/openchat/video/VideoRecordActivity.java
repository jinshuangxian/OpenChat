package com.weicong.openchat.video;

import java.io.File;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.TextView;

import com.weicong.openchat.R;

public class VideoRecordActivity extends Activity {

	public static final String FILE_PATH = "file_path";
	
    private VideoRecorderView recorderView;
    private Button videoController;
    private boolean isCancel = false;
    
    private File mVideoFile = null;
    
    private TextView mCenterTitle;
    
    private MenuItem mSendItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_record);
        initTitleCenterActionBar();
        getActionBar().setDisplayHomeAsUpEnabled(true);

        recorderView = (VideoRecorderView) findViewById(R.id.recorder);
        videoController = (Button) findViewById(R.id.videoController);

        ViewGroup.LayoutParams params = recorderView.getLayoutParams();
        int[] dev = getResolution(this);
        params.width = dev[0];
        params.height = (int) (((float) dev[0]));
        recorderView.setLayoutParams(params);
        videoController.setOnTouchListener(new VideoTouchListener());

        recorderView.setRecorderListener(new VideoRecorderView.RecorderListener() {

            @Override
            public void recording(int maxtime, int nowtime) {

            }

            @Override
            public void recordSuccess(File videoFile) {
            	mVideoFile = videoFile;
            	mSendItem.setVisible(true);
                releaseAnimations();
            }

            @Override
            public void recordStop() {
            	
            }

            @Override
            public void recordCancel() {
                releaseAnimations();
            }

            @Override
            public void recordStart() {
                
            }

            @Override
            public void videoStop() {
                
            }

            @Override
            public void videoStart() {
                
            }


        });

    }
    
    private void initTitleCenterActionBar() {
		LayoutInflater inflator = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View centerTitle = inflator.inflate(R.layout.actionbar_title, null);
		mCenterTitle = (TextView) centerTitle.findViewById(R.id.actionbar_title);
		ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowCustomEnabled(true); //使自定义的普通View能在title栏显示
		actionBar.setCustomView(centerTitle,layoutParams);
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.send_menu, menu);
    	mSendItem = menu.findItem(R.id.action_send);
    	mSendItem.setVisible(false);
    	return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			back();
			return true;
		} else if (item.getItemId() == R.id.action_send) {
			Intent intent = new Intent();
			intent.putExtra(FILE_PATH, mVideoFile.getAbsolutePath());
			setResult(RESULT_OK, intent);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		back();
	}
	
	/**
	 * 返回
	 */
	private void back() {
		if (mVideoFile != null) {
			mVideoFile.delete();
		}
		finish();
	}
	
	private int[] getResolution(Context context) {
		int resolution[] = new int[2];
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager windowManager = null;
		windowManager=(WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(dm);
		resolution[0] = dm.widthPixels;
		resolution[1] = dm.heightPixels;
		return resolution;
	}
    
    public class VideoTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {


            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    recorderView.startRecord();
                    isCancel = false;
                    pressAnimations();
                    showPressMessage();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (event.getX() > 0
                            && event.getX() < videoController.getWidth()
                            && event.getY() > 0
                            && event.getY() < videoController.getHeight()) {
                        showPressMessage();
                        isCancel = false;
                    } else {
                        cancelAnimations();
                        isCancel = true;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                	videoController.setText("按住拍");
                    if (isCancel) {
                        recorderView.cancelRecord();
                    }else{
                        recorderView.endRecord();
                    }
                    mCenterTitle.setVisibility(View.GONE);
                    releaseAnimations();
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    /**
     * 移动取消弹出动画
     */
    public void cancelAnimations() {
    	mCenterTitle.setBackgroundColor(getResources().getColor(android.R.color.transparent));
    	mCenterTitle.setTextColor(getResources().getColor(R.color.red));
    	mCenterTitle.setText("松手取消");
        videoController.setText("松手取消");
    }

    /**
     * 显示提示信息
     */
    public void showPressMessage() {
    	mCenterTitle.setVisibility(View.VISIBLE);
    	mCenterTitle.setBackgroundColor(getResources().getColor(android.R.color.transparent));
    	mCenterTitle.setTextColor(getResources().getColor(R.color.green));
    	mCenterTitle.setText("上移取消");
        videoController.setText("上移取消");
    }


    /**
     * 按下时候动画效果
     */
    public void pressAnimations() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.5f,
                1, 1.5f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(200);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setDuration(200);

        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setFillAfter(false);

        videoController.startAnimation(animationSet);
    }

    /**
     * 释放时候动画效果
     */
    public void releaseAnimations() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.5f, 1f,
                1.5f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(200);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
        alphaAnimation.setDuration(200);

        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.setFillAfter(true);

        mCenterTitle.setVisibility(View.GONE);
        videoController.startAnimation(animationSet);
    }


}
