package com.weicong.openchat.video;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.VideoView;

import com.weicong.openchat.R;

public class PlayVideoActivity extends Activity {

	public static final String PATH = "path";
	private VideoView mVideoView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_video);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("��Ƶ");
		
		String path = getIntent().getStringExtra(PATH);
		
		mVideoView = (VideoView) findViewById(R.id.play_video);
        mVideoView.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer arg0) {
//				finish();
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mVideoView.start();
			}
		});
		
        mVideoView.setVideoPath(path);
        mVideoView.start();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
