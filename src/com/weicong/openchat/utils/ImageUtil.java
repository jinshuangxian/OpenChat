package com.weicong.openchat.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;

public class ImageUtil {

	public static final String IMAGE_TAG = "!#rtecdsdffdgssasdf!#";
	
	public static String getImageSendMsg(String filePath) {
		try {
			File file = new File(filePath);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			byte[] data = new byte[1024];
			FileInputStream fis = new FileInputStream(file); 
			int len = 0;
			while((len = fis.read(data)) != -1) {               
				byteArrayOutputStream.write(data, 0, len);           
			}       
			String content = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 
					Base64.DEFAULT);
			fis.close();
			byteArrayOutputStream.close();
			StringBuilder sb = new StringBuilder();
			sb.append(content);
			sb.append(IMAGE_TAG);
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getImageMsg(String filePath) {
		return  filePath + IMAGE_TAG;
	}
	
	public static String getImageContentPath(String msg) {
		int end = msg.lastIndexOf(IMAGE_TAG);
		return msg.substring(0, end);
	}
	
	public static String saveImage(String msg) {
		int end = msg.lastIndexOf(IMAGE_TAG);
		String content = msg.substring(0, end);
		byte[] data = Base64.decode(content, Base64.DEFAULT);
		String dir = Environment.getExternalStorageDirectory()
				+ "/openchat/image";
		File fileDir = new File(dir);
		if (!fileDir.exists())
			fileDir.mkdirs();
		String fileName = generateFileName();
		File file = new File(fileDir, fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		msg = file.getAbsolutePath() + IMAGE_TAG;
		
		return msg;
	}
	
	public static Bitmap compressImageFromFile(String filePath, float width, float height) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(filePath, newOpts);
		
		newOpts.inJustDecodeBounds = false;
		
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		
		int be = 1;
		if (w > h && w > width) {
			be = (int)(newOpts.outWidth / width);
			
		} else if (w < h && h > height) {
			be = (int)(newOpts.outHeight / height);
		}
		
		if (be <= 0) {
			be = 1;
		}
		
		newOpts.inSampleSize = be;
		newOpts.inPreferredConfig = Config.ARGB_8888;
		newOpts.inPurgeable = true;
		newOpts.inInputShareable = true;
		
		bitmap = BitmapFactory.decodeFile(filePath, newOpts);
		
		return bitmap;
	}
	
	/**
	 * 随机生成文件名称
	 * 
	 * @return
	 */
	private static String generateFileName() {
		return UUID.randomUUID().toString() + ".jpg";
	}
}
